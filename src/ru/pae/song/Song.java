package ru.pae.song;

public class Song {
    private String name;
    private String author;
    private int duration;
    Song(String name, String author, int duration) {
        this.name = name;
        this.author = author;
        this.duration = duration;
    }


    Song() {
        this("Не указано", "Не указано", 0);
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public int getDuration() {
        return duration;
    }

    public String category() {
        String category = new String();
        if (duration < 120 ) {
            category = "short";
        }
        if (duration > 240 ) {
            category = "long";
        }
        if( duration >120 && duration < 240 ){
            category = "medium";
        }
        return category;
    }

    boolean isSameCategory(Song song){
        return this.category().equals(song.category());
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", duration=" + duration +
                '}';
    }
}


